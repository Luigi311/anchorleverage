import base64

from terra_sdk.core.coins import Coins
from terra_sdk.core.coins import Coin

from src.functions import logger, requests_retry_session


def swap_token_terraswap(
    terra_instance,
    amount,
    from_token_addr,
    to_token_addr,
    contract_pair_addr,
    belief_price,
):
    try:
        if check_tokens_contract_terraswap(
            terra_instance, from_token_addr, to_token_addr, contract_pair_addr
        ):
            denominator = 10 ** 6

            token_from_native = terra_instance.check_native(from_token_addr)

            contract_address = ""
            coins = Coins()
            msg = {}

            if token_from_native:
                contract_address = contract_pair_addr
                coin = Coin(from_token_addr, int(amount * denominator)).to_data()
                coins = Coins.from_data([coin])
                msg = {
                    "swap": {
                        "max_spread": str(terra_instance.max_spread),
                        "belief_price": str(belief_price),
                        "offer_asset": {
                            "info": {"native_token": {"denom": str(from_token_addr)}},
                            "amount": str(int(amount * denominator)),
                        },
                    }
                }
            else:
                denominator = terra_instance.get_token_denominator(from_token_addr)
                contract_address = from_token_addr
                msg = {
                    "send": {
                        "msg": base64.b64encode(
                            str.encode(
                                '{"swap":{"max_spread":"'
                                + str(terra_instance.max_spread)
                                + '","belief_price":"'
                                + str(belief_price)
                                + '"}}'
                            )
                        ).decode("utf-8"),
                        "amount": str(int(amount * denominator)),
                        "contract": str(contract_pair_addr),
                    }
                }

            output = terra_instance.contract_executor(contract_address, msg, coins)
            logger(f"Terraswap TXHASH: {output}", log_type=0)

    except:
        raise Exception(
            f"Error swappping {from_token_addr} to {to_token_addr} via {contract_pair_addr} on terraswap"
        )


def check_tokens_contract_terraswap(
    terra_instance, token_1_addr, token_2_addr, contract_addr
):
    # Check if token_1_addr and token_2_addr are valid for contract_addr
    pairs = get_pairs_terraswap(terra_instance)

    contract_addresses = pairs[contract_addr]["addresses"]

    if token_1_addr in contract_addresses and token_2_addr in contract_addresses:
        return True

    raise Exception(
        f"Invalid tokens for contract\nFrom token: {token_1_addr}\nTo token: {token_2_addr}\nContract: {contract_addr}"
    )


def get_tokens_terraswap(terra_instance):
    output = {}

    tokens_url = terra_instance.terraswap_api + "/tokens"
    tokens_response = (
        requests_retry_session()
        .get(url=tokens_url, timeout=terra_instance.timeout)
        .json()
    )

    for token in tokens_response:
        output[token["contract_addr"]] = token["symbol"]

    return output


def get_pairs_terraswap(terra_instance):
    output = {}

    terra_tokens = get_tokens_terraswap(terra_instance)
    pairs_url = terra_instance.terraswap_api + "/pairs"
    pairs_response = (
        requests_retry_session()
        .get(url=pairs_url, timeout=terra_instance.timeout)
        .json()
    )

    for pair in pairs_response["pairs"]:
        token_pair_symbols = []
        token_pair_addr = []
        for token in pair["asset_infos"]:
            if "native_token" in token:
                token_pair_symbols.append(token["native_token"]["denom"])
                token_pair_addr.append(token["native_token"]["denom"])
            else:
                token_addrs = token["token"]["contract_addr"]
                if token_addrs in terra_tokens:
                    token_pair_symbols.append(terra_tokens[token_addrs])
                    token_pair_addr.append(token_addrs)
                else:
                    token_pair_symbols.append(token_addrs)
                    token_pair_addr.append(token_addrs)

        output[pair["contract_addr"]] = {
            "symbols": token_pair_symbols[0] + "-" + token_pair_symbols[1],
            "addresses": [token_pair_addr[0], token_pair_addr[1]],
        }

    return output


def get_belief_price_terraswap(
    terra_instance, amount, from_token_addr, to_token_addr, contract_pair_addr
):
    try:
        if check_tokens_contract_terraswap(
            terra_instance, from_token_addr, to_token_addr, contract_pair_addr
        ):
            token_from_native = terra_instance.check_native(from_token_addr)

            denominator = 10 ** 6

            if token_from_native:
                msg = {
                    "simulation": {
                        "offer_asset": {
                            "amount": str(int(amount * denominator)),
                            "info": {"native_token": {"denom": str(from_token_addr)}},
                        }
                    }
                }
            else:
                denominator = terra_instance.get_token_denominator(from_token_addr)
                msg = {
                    "simulation": {
                        "offer_asset": {
                            "amount": str(int(amount * denominator)),
                            "info": {"token": {"contract_addr": str(from_token_addr)}},
                        }
                    }
                }

            query_output = terra_instance.contract_query(contract_pair_addr, msg)

            output = (amount * denominator) / int(query_output["return_amount"])

            return output
    except:
        raise Exception(
            f"Error getting belief price for {from_token_addr} to {to_token_addr} via {contract_pair_addr} on terraswap"
        )
