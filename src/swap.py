from src.functions import logger
from src.terraswap import swap_token_terraswap, get_belief_price_terraswap
from src.astroport import swap_token_astroport, get_belief_price_astroport
from src.anchor import anchor_earn_deposit, get_anchor_asset_borrow_limit


def swap_anc_to_ust(terra_instance, anc_amount):
    swap_terraswap_vs_astroport(
        terra_instance, anc_amount, terra_instance.anc_token, "uusd"
    )


def swap_ust_to_luna(terra_instance, ust_amount):
    swap_terraswap_vs_astroport(terra_instance, ust_amount, "uusd", "uluna")


def swap_luna_to_ust(terra_instance, luna_amount):
    swap_terraswap_vs_astroport(terra_instance, luna_amount, "uluna", "uusd")


def swap_luna_to_bluna(terra_instance, luna_amount):
    swap_terraswap_vs_astroport(
        terra_instance, luna_amount, "uluna", terra_instance.bLUNA_token
    )


def swap_bluna_to_luna(terra_instance, bluna_amount):
    swap_terraswap_vs_astroport(
        terra_instance, bluna_amount, terra_instance.bLUNA_token, "uluna"
    )


def swap_ust_to_bluna(terra_instance, ust_amount):
    logger(f"Swapping {ust_amount} UST to LUNA", log_type=0)
    swap_ust_to_luna(terra_instance, ust_amount)

    luna_amount = terra_instance.get_account_native_balance()["luna"]

    logger(f"Swapping {luna_amount} Luna to bLuna", log_type=0)
    swap_luna_to_bluna(terra_instance, luna_amount)


def swap_bluna_to_ust(terra_instance, bluna_amount):
    logger(f"Swapping {bluna_amount} bLuna to luna", log_type=0)
    swap_bluna_to_luna(terra_instance, bluna_amount)

    luna_amount = terra_instance.get_account_native_balance()["luna"]

    logger(f"Swapping {luna_amount} luna to ust", log_type=0)
    swap_luna_to_ust(terra_instance, luna_amount)


def swap_terraswap_vs_astroport(terra_instance, amount, from_token_addr, to_token_addr):
    try:
        if "uusd" in [from_token_addr, to_token_addr] and "uluna" in [
            from_token_addr,
            to_token_addr,
        ]:
            contract_terraswap = terra_instance.ust_luna_contract_terraswap
            contract_astroport = terra_instance.ust_luna_contract_astroport

        elif "uluna" in [
            from_token_addr,
            to_token_addr,
        ] and terra_instance.bLUNA_token in [from_token_addr, to_token_addr]:
            contract_terraswap = terra_instance.luna_bluna_contract_terraswap
            contract_astroport = terra_instance.luna_bluna_contract_astroport

        elif terra_instance.anc_token in [
            from_token_addr,
            to_token_addr,
        ] and "uusd" in [from_token_addr, to_token_addr]:
            contract_terraswap = terra_instance.anc_ust_contract_terraswap
            contract_astroport = terra_instance.anc_ust_contract_astroport

        else:
            raise Exception(
                f"Swap from {from_token_addr} to {to_token_addr} is not supported"
            )

        belief_price_terraswap = get_belief_price_terraswap(
            terra_instance,
            amount,
            from_token_addr,
            to_token_addr,
            contract_terraswap,
        )

        belief_price_astroport = get_belief_price_astroport(
            terra_instance,
            amount,
            from_token_addr,
            to_token_addr,
            contract_astroport,
        )

        if belief_price_terraswap < belief_price_astroport:
            logger(
                f"Swapping {amount} {from_token_addr} to {to_token_addr} via terraswap",
                log_type=0,
            )

            # Swap via terraswap
            swap_token_terraswap(
                terra_instance,
                amount,
                from_token_addr,
                to_token_addr,
                contract_terraswap,
                belief_price_terraswap,
            )
        else:
            logger(
                f"Swapping {amount} {from_token_addr} to {to_token_addr} via astroport",
                log_type=0,
            )

            # Swap via astroport
            swap_token_astroport(
                terra_instance,
                amount,
                from_token_addr,
                to_token_addr,
                contract_astroport,
                belief_price_astroport,
            )

    except:
        raise Exception(f"Swap from {from_token_addr} to {to_token_addr} failed")


def check_leverage_ratio(
    calculated_leverage_ratio,
    target_leverage_ratio,
    leverage_swapping_ratio_tmp,
    percentage,
):
    output = leverage_swapping_ratio_tmp

    if (
        (target_leverage_ratio - (target_leverage_ratio * percentage))
        <= calculated_leverage_ratio
        <= (target_leverage_ratio + (target_leverage_ratio * percentage))
    ):
        # leverage ratio is within % of target leverage ratio
        return output
    elif calculated_leverage_ratio < target_leverage_ratio:
        # leverage ratio is too low
        output = output - (percentage / 10)
    else:
        # leverage ratio is too high
        output = output + (percentage / 10)

    return output


def get_leverage_swapping_ratio(terra_instance):
    # Just a random number used for calculations
    amount = 1000

    borrow_limit = get_anchor_asset_borrow_limit(
        terra_instance, terra_instance.bLUNA_token
    )
    target_ltb = terra_instance.anchor_usage_target_percentage
    earn_borrow_ratio = terra_instance.ust_earn_to_borrow_target_ratio

    target_borrow = amount / earn_borrow_ratio
    collateral_amount = target_borrow / target_ltb / borrow_limit

    ratio = collateral_amount / amount

    return ratio


def get_leverage_amounts(terra_instance, ust_amount):
    earn_amount = ust_amount / (1 + terra_instance.leverage_swapping_ratio)
    collateral_amount = ust_amount - earn_amount
    logger(
        f"Earn amount: {earn_amount}, collateral amount {collateral_amount}", log_type=1
    )

    if earn_amount < 0 or collateral_amount < 0:
        raise Exception("Leverage amounts are negative")

    return earn_amount, collateral_amount


def leverage_up(terra_instance, use_earn_amount, use_luna_amount):
    if use_earn_amount > 0:
        logger(f"Depositing {use_earn_amount} into anchor earn", log_type=0)
        txhash = anchor_earn_deposit(terra_instance, use_earn_amount)
        logger(f"Deposited UST into Anchor Earn TXHASH: {txhash}", log_type=0)

    if use_luna_amount > 0:
        logger(f"Buying ${use_luna_amount} worth of bluna", log_type=0)
        swap_ust_to_bluna(terra_instance, use_luna_amount)
