import os

from telegram import Update, Bot, BotCommand
from telegram.ext import Updater, CommandHandler, Filters, CallbackContext
from dotenv import load_dotenv
from src.terra_functions import Terra

load_dotenv(override=True)

telegram_chat_id = os.getenv("TELEGRAM_CHAT_ID")
token = os.getenv("TELEGRAM_TOKEN")

if telegram_chat_id is not None and telegram_chat_id.strip() != "":
    telegram_chat_id = int(telegram_chat_id)


def ping_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /ping is issued."""
    update.message.reply_text("pong")


def help_command(update: Update, context: CallbackContext) -> None:
    """Send list of commands when /help is issued."""
    update.message.reply_text("Commands:\n/ping check if the bot is online\n")


def setup_bot(terra_instance: Terra) -> None:
    print("Starting up telegram bot")
    try:
        # Create the Updater and pass it your bot's token.
        updater = Updater(token, use_context=True)
        bot = Bot(token)

        Commands = {
            1: BotCommand("help", "help"),
            2: BotCommand("ping", "ping"),
        }
        bot.set_my_commands(list(Commands.values()))

        # Get the dispatcher to register handlers
        dispatcher = updater.dispatcher

        # on different commands - answer in Telegram
        dispatcher.add_handler(
            CommandHandler(
                "help", help_command, filters=Filters.chat(chat_id=telegram_chat_id)
            )
        )
        dispatcher.add_handler(
            CommandHandler(
                "ping", ping_command, filters=Filters.chat(chat_id=telegram_chat_id)
            )
        )

        # Start the Bot
        updater.start_polling()

    except Exception as e:
        raise Exception(f"Telegram bot error: {e}")
