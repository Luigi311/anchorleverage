FROM python:3
WORKDIR /app
COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

# General
# Seconds between each check
ENV SLEEP_DURATION 600
# Timeout limits for get requests
ENV TIMEOUT_DURATION 5
ENV DEBUG False 

# Configuration
ENV MNEMONIC ""
ENV CHAIN_ID "bombay-12"
#ENV PUBLIC_LCD_URL "https://lcd.terra.dev" # Uncomment to hardcode your own LCD server
#ENV PUBLIC_FCD_URL "https://fcd.terra.dev" # Uncomment to hardcode your own LCD server

# Wallet Configuration
ENV WALLET_MIN_UST_BALANCE 10
ENV WALLET_MAX_UST_BALANCE 20
ENV MAX_SPREAD = 0.01

# Anchor Configuration
ENV ANCHOR_USAGE_TARGET_PERCENTAGE 65
ENV ANCHOR_USAGE_MIN_PERCENTAGE 60
ENV ANCHOR_USAGE_MAX_PERCENTAGE 75
ENV ANCHOR_ANC_MAX_BALANCE 20
ENV UST_EARN_TO_BORROW_TARGET_RATIO 80
ENV UST_EARN_TO_BORROW_MIN_RATIO 20
ENV UST_EARN_TO_BORROW_MAX_RATIO 90
ENV DELEVERAGE_BORROW_USAGE_TARGET 90

CMD ["python", "-u", "main.py"]